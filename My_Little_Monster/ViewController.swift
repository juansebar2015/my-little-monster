//
//  ViewController.swift
//  My_Little_Monster
//
//  Created by Juan Ramirez on 4/25/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    @IBOutlet weak var monsterImage: MonsterImage!
    @IBOutlet weak var heartImage: DragImage!
    @IBOutlet weak var foodImage: DragImage!
    
    @IBOutlet weak var penalty1Image: UIImageView!
    @IBOutlet weak var penalty2Image: UIImageView!
    @IBOutlet weak var penalty3Image: UIImageView!
    
    @IBOutlet weak var playAgainLabel: UILabel!
    @IBOutlet weak var playAgainButton: UIButton!
    
    
    let DIM_ALPHA: CGFloat = 0.2
    let OPAQUE: CGFloat = 1.0
    let MAX_PENALTIES = 3
    
    var currPenalties = 0
    var timer: NSTimer!     //Means we know were gonna have one of these for sure
    var monsterHappy = false
    var gameOverState: Bool = false
    var currentItem: UInt32 = 0
    
    var musicPlayer : AVAudioPlayer!
    var sfxBite : AVAudioPlayer!
    var sfxHeart : AVAudioPlayer!
    var sfxDeath : AVAudioPlayer!
    var sfxSkull : AVAudioPlayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        playAgainLabel.hidden = true
        playAgainButton.hidden = true
        
        foodImage.dropTarget = monsterImage
        heartImage.dropTarget = monsterImage
        
        penalty1Image.alpha = DIM_ALPHA
        penalty2Image.alpha = DIM_ALPHA
        penalty3Image.alpha = DIM_ALPHA
        
        do{
            try musicPlayer = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("cave-music", ofType: "mp3")!))
            
            try sfxBite = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("bite", ofType: "wav")!))
            
            try sfxHeart = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("heart", ofType: "wav")!))
            
            try sfxDeath = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("death", ofType: "wav")!))
            
            try sfxSkull = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("skull", ofType: "wav")!))
            
            musicPlayer.prepareToPlay()
            musicPlayer.play()
            
            sfxBite.prepareToPlay()
            sfxHeart.prepareToPlay()
            sfxDeath.prepareToPlay()
            sfxSkull.prepareToPlay()
            
        } catch let error as NSError{
            print(error.debugDescription)
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "itemDroppedOnCharacter:", name: "onTargetDropped", object: nil)
        
        startTimer()
        
    }

    func itemDroppedOnCharacter(notif: AnyObject){
        
        monsterHappy = true
        startTimer() // Reset the timer
        
        if currentItem == 0 {
            sfxHeart.play()
        } else {
            sfxBite.play()
        }
        
        foodImage.alpha = DIM_ALPHA
        foodImage.userInteractionEnabled = false
        heartImage.alpha = DIM_ALPHA
        heartImage.userInteractionEnabled = false
        
        
    }
  
    func startTimer() {
        if timer != nil {
            timer.invalidate()  //Stop existing timer
            
        }
        
        timer = NSTimer.scheduledTimerWithTimeInterval(3.0, target: self, selector: "changeGameState", userInfo: nil, repeats: true)
    }
    
    func changeGameState() {
        
        if !monsterHappy {
            currPenalties += 1
            
            sfxSkull.play()
            
            if currPenalties == 1 {
                penalty1Image.alpha = OPAQUE
                penalty2Image.alpha = DIM_ALPHA
            } else if currPenalties == 2 {
                penalty2Image.alpha = OPAQUE
                penalty3Image.alpha = DIM_ALPHA
            } else if currPenalties >= 3 {
                penalty3Image.alpha = OPAQUE
            } else {
                penalty1Image.alpha = DIM_ALPHA
                penalty2Image.alpha = DIM_ALPHA
                penalty3Image.alpha = DIM_ALPHA
            }
            
            if currPenalties >= MAX_PENALTIES{
                gameOver()
            }
        }
        
        if !gameOverState {
        
            let rand = arc4random_uniform(2)    // 0 or 1
            
            if rand == 0 {  //Make heart active
                foodImage.alpha = DIM_ALPHA
                foodImage.userInteractionEnabled = false
                
                heartImage.alpha = OPAQUE
                heartImage.userInteractionEnabled = true
            } else {        //Make food active
                heartImage.alpha = DIM_ALPHA
                heartImage.userInteractionEnabled = false
                
                foodImage.alpha = OPAQUE
                foodImage.userInteractionEnabled = true
            }
            
            currentItem = rand
            monsterHappy = false   //New item monster wants so he's not happy
        }
    }
    
    func gameOver() {
        gameOverState = true
        timer.invalidate()
        monsterImage.playDeathAnimation()
        sfxDeath.play()
        
        //Invalidate movement of items
        foodImage.userInteractionEnabled = false
        heartImage.userInteractionEnabled = false
        
        playAgainLabel.hidden = false
        playAgainButton.hidden = false
        
        
    }
    
    func restartGame() {
        playAgainLabel.hidden = true
        playAgainButton.hidden = true
        
        //Reset variables
        currPenalties = 0
        currentItem = 0
        monsterHappy = false
        gameOverState = false
        
        //Reset the animation
        monsterImage.playReviveAnimation()
        //monsterImage.playIdleAnimation()
        performSelector("idleAnimation", withObject: nil, afterDelay: 1)
        
        //Reset skulls
        penalty1Image.alpha = DIM_ALPHA
        penalty2Image.alpha = DIM_ALPHA
        penalty3Image.alpha = DIM_ALPHA
        
        
        //performSelector(Selector(startTimer()), withObject: nil, afterDelay: 1)
        performSelector("startTimer", withObject: nil, afterDelay: 0.9) //startTimer()
    }
    
    func idleAnimation() {
        monsterImage.playIdleAnimation()
    }
    
    @IBAction func playAgain(sender: AnyObject) {
        
        restartGame()
    }

}

