//
//  DragImage.swift
//  My_Little_Monster
//
//  Created by Juan Ramirez on 4/26/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import Foundation
import UIKit    //contains information for all iOS UI controls

class DragImage : UIImageView {
    
    var originalPostition : CGPoint!
    var dropTarget: UIView?
    
    
    //Must include these initializers
    override init(frame: CGRect) {
        super.init(frame: frame)    //Must call super.init
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder) //Just pass it to super(parent initializer)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        originalPostition = self.center  //Save the center position of the imageView being touched
        //print("INSIDE THE OTHER Began Touch method!!")
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {//event could be optional b/c of ?
        
        if let touch = touches.first {  //Grab the first item in the touches Set
            let position = touch.locationInView(self.superview) //Get location of touch in superview
            self.center = CGPointMake(position.x, position.y)
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        if let touch = touches.first, let target = dropTarget {
            
            let position = touch.locationInView(self.superview)
                            //Monster UIImage Frame
            if CGRectContainsPoint(target.frame, position) { //If position is inside Monster UIImage frame
                NSNotificationCenter.defaultCenter().postNotification(NSNotification(name: "onTargetDropped", object: nil))
            }
        }
        
        self.center = originalPostition
        
        
    }
}