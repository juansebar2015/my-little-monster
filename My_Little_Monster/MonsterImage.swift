//
//  MonsterImage.swift
//  My_Little_Monster
//
//  Created by Juan Ramirez on 4/30/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import Foundation
import UIKit

class MonsterImage: UIImageView {
    
    //Required initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        playIdleAnimation()
    }
    
    
    func playIdleAnimation(){
        
        self.image = UIImage(named: "idle1.png")
        self.animationImages = nil
        
        
        var imArray = [UIImage]()
        
        //for var x = 1; x <= 4; x+=1 {
        for x in 1...4 {
            let image = UIImage(named: "idle\(x).png")
            imArray.append(image!)
        }
        
        self.animationImages = imArray
        self.animationDuration = 0.8    //How long animation plays
        self.animationRepeatCount = 0   //"0" makes it do it infinitively
        self.startAnimating()
    }
    
    func playDeathAnimation() {
        
        self.image = UIImage(named: "dead5.png")    //Set default image
        self.animationImages = nil  //Empties all animation images
        
        var imArray = [UIImage]()
        
        //for var x = 1; x <= 4; x+=1 {
        for x in 1...4 {
            let image = UIImage(named: "dead\(x).png")
            imArray.append(image!)
        }
        
        self.animationImages = imArray
        self.animationDuration = 0.8    //How long animation plays
        self.animationRepeatCount = 1
        self.startAnimating()
    }
    
    func playReviveAnimation() {
        self.image = UIImage(named: "idle1.png")
        self.animationImages = nil
        
        var imArray = [UIImage]()
        
        for x in 1...4 {
            let image = UIImage(named: "dead\((x-5)*(-1)).png")
            imArray.append(image!)
        }
        
        self.animationImages = imArray
        self.animationDuration = 0.8
        self.animationRepeatCount = 1
        self.startAnimating()
    }
}